import pandas as pd 
from Pokemon import Pokemon
from PokemonList import PokemonList
from Player import Player


def main(): 
  
  pokemondict = PokemonList()
  player = Player()
  print(player)
  player.player_party.generate_opponent()
  player.player_party.sort_party_by_modified_attack_value()
  fighter = player.player_party.party[0]
  opponent = fighter.opponent
  while fighter.health and opponent.health > 0: 
      opponent.health = opponent.health - fighter.against_value
      fighter.health = fighter.health - opponent.against_value
      if fighter.health <= 0:
        print("Your Pokemon has lost the battle! He needs time to rest.")
        break
      if opponent.health <= 0:
        print("Your Pokemon has WON the battle! Go celebrate.")

  
 


if __name__ == "__main__":
  main()
  
