import Pokemon
import random
from PokemonList import PokemonList


class Player:
  
  def __init__(self):
      self.player_party = self.pick_party()       
      self.sorted_party = self.order_party()
      
  
  
  def pick_party(self):
    #cmd_line_user_selected_pokemon = input("Which five pokemon do you want in your party. Enter them with a comma between: ") 
    cmd_line_user_selected_pokemon = "Weedle, Ivysaur, Pidgeot, Rattata, Pikachu"
    pokemonlist = PokemonList()  
    pokemonlist.create_party(cmd_line_user_selected_pokemon)
    return pokemonlist   
      
  def order_party(self):
    return self.player_party.sort_party()  


  def __str__(self):
    print("Here is your Pokemon party, in order of strength:")  
    for pokemon in self.player_party.party:
      print(pokemon)
    return ""

  def againsttype(self):
    #TODO develop the against_type method based on target pokemon
    return 0

