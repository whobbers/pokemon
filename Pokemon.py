import pandas as pd


class Pokemon:
  def __init__(self, name, sp_attack, primary_type, secondary_type, health, against_bug,
             against_dark, against_dragon, against_electric, against_fairy, 
              against_fight, against_fire, against_flying, against_ghost, against_grass, 
              against_ground, against_ice, against_normal, against_poison, against_psychic, 
              against_rock, against_steel, against_water):
    self.name = name
    self.health = health
    self.sp_attack = sp_attack
    self.primary_type = primary_type
    self.secondary_type = secondary_type
    self.opponent = None
    self.against = {}
    self.against_value = 0
    self.against['bug'] = against_bug
    self.against['dark'] = against_dark
    self.against['dragon'] = against_dragon
    self.against['electric'] =against_electric
    self.against['fairy'] = against_fairy
    self.against['fighting'] = against_fight
    self.against['fire'] = against_fire
    self.against['flying'] = against_flying
    self.against['ghost'] = against_ghost
    self.against['grass'] = against_grass
    self.against['ground'] = against_ground
    self.against['ice'] = against_ice
    self.against['normal'] = against_normal
    self.against['poison'] = against_poison
    self.against['psychic'] = against_psychic
    self.against['rock'] = against_rock
    self.against['steel'] = against_steel
    self.against['water'] = against_water
    

  def __str__(self):
    return self.name + ":" + self.sp_attack.astype(str)

  def add_opponent(self, opponent):
    self.opponent = opponent
    val2 = 0
    val1 = self.against[self.opponent.primary_type] * self.sp_attack.astype(int) 
    if pd.notna(self.opponent.secondary_type):
      val2 = self.against[self.opponent.secondary_type] * self.sp_attack.astype(int) 
    self.against_value = max(val1,val2)
  
  def get_pokemon_attack_value(self, pokemon_opponent):
    return pokemon_opponent.against[self.primary_type] * self.sp_attack.T
   