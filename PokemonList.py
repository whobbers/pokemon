import pandas as pd
import random as random
from Pokemon import Pokemon

class PokemonList:
  def __init__(self):
    self.p_dict = self.pokemon_dict()
    self.party = {}
 
  def pokemon_dict(self):
    pokemoncsv = pd.read_csv("pokemon.csv") 
    all_pokemon = {}
    for idx in pokemoncsv.index: 
      name = pokemoncsv["name"][idx]
      sp_attack =  pokemoncsv['sp_attack'][idx]
      primary_type = pokemoncsv['type1'][idx]
      secondary_type = pokemoncsv['type2'][idx]
      against_bug = pokemoncsv['against_bug'][idx]
      against_dark = pokemoncsv['against_dark'][idx]
      against_dragon = pokemoncsv['against_dragon'][idx]
      against_electric = pokemoncsv['against_electric'][idx]
      against_fairy = pokemoncsv['against_fairy'][idx]
      against_fight = pokemoncsv['against_fight'][idx]
      against_fire = pokemoncsv['against_fire'][idx]
      against_flying = pokemoncsv['against_flying'][idx]
      against_ghost = pokemoncsv['against_ghost'][idx]
      against_grass = pokemoncsv['against_grass'][idx]
      against_ground = pokemoncsv['against_ground'][idx]
      against_ice = pokemoncsv['against_ice'][idx]
      against_normal = pokemoncsv['against_normal'][idx]
      against_poison = pokemoncsv['against_poison'][idx]
      against_psychic = pokemoncsv['against_psychic'][idx]
      against_rock = pokemoncsv['against_rock'][idx]
      against_steel = pokemoncsv['against_steel'][idx]
      against_water = pokemoncsv['against_water'][idx]
      health = pokemoncsv['hp'][idx]
      pokemon_obj = Pokemon(name, sp_attack.T, primary_type, secondary_type, health, against_bug,
             against_dark, against_dragon, against_electric, against_fairy, 
              against_fight, against_fire, against_flying, against_ghost, against_grass, 
              against_ground, against_ice, against_normal, against_poison, against_psychic, 
              against_rock, against_steel, against_water)
      all_pokemon[pokemon_obj.name] = pokemon_obj
    return all_pokemon

  def create_party(self, pokemon_names):
    pokemon_array = pokemon_names.replace(' ','').split(",")
    for item in pokemon_array:
      self.party[item] =  self.p_dict.get(item)
    
  def sort_party(self):
    self.party = sorted(self.party.values(), 
        key=lambda pokemon: pokemon.sp_attack, reverse=True)

  def sort_party_by_modified_attack_value(self):
    self.party = sorted(self.party, 
        key=lambda pokemon: pokemon.against_value, reverse=True)

  
  def generate_opponent(self):
    random_number = random.randint(0,len(self.p_dict))
    opponent = list(self.p_dict.items())[random_number][1]
    for pokemon in self.party:
      pokemon.add_opponent(opponent)



  #Just return sorted party without changing the list
  def sorted_party(self):
    return sorted(self.party.values(), key=lambda pokemon: pokemon.sp_attack)

    